import { BrowserRouter, Route, Routes } from "react-router-dom";
import Home from "./Pages/Home"
import Login from "./Components/Login/Login"
import Signup from "./Components/Signup/Register"
import { ToastContainer } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'
import { useEffect, useState } from "react";
import UserRoles from "./Pages/UserRoles";
import BlogHome from "./Pages/BlogHome";
import SinglePost from "./Components/SinglePost/SingleBlog";
import Navbar from "./Components/Navbar/Navbar";
import CreateBlog from "./Pages/CreateBlog";
import MasterServicesHome from "./Pages/VendorOptions";
import VendorsHome from "./Pages/VendorsHome";
import VendorsListHome from "./Pages/VendorsListHome";
import VendorProfile from "./Components/VendorProfile/VendorProfile"
import PlannerHome from "./Pages/PlannerHome";
import MultiStepForm from "./Pages/MultiStepForm";
import VendorProfileHome from "./Pages/VendorProfileHome";
import Orders from "./Components/ClientDashBoard/ClientTable/ClientTable"
import VendorDashboardProfile from "./Components/ClientDashBoard/ProfileInfo/ProfileInfo"
import VendorDashboardProducts from "./Pages/VendorProduct"
import OrdersHome from "./Components/ClientDashBoard/OrdersHome/OrdersHome"
import CustomerDashboardHome from "./Pages/CustomerDashboardHome";
import CustomerOrders from "./Components/CustomerDashboard/CustomerOrders/CustomerOrders"
import Cart from "./Components/Cart/Cart"
import Checkout from "./Components/Checkout/Checkout"
import AdminHome from "./Pages/AdminDashBoardPages/Home/Home"
import UserList from "./Pages/AdminDashBoardPages/userList/UserList"
import VendorsList from "./Pages/AdminDashBoardPages/ProductList/ProductList"
import User from "./Pages/AdminDashBoardPages/user/User"
import Vendor from "./Pages/AdminDashBoardPages/Product/Product"
import NewUser from "./Pages/AdminDashBoardPages/NewUser/NewUser"
import NewVendor from "./Pages/AdminDashBoardPages/NewProduct/NewProduct"
import BookingsList from "./Pages/AdminDashBoardPages/Bookings/BookingsList"
import CategoriesList from "./Pages/AdminDashBoardPages/Categories/Categories"
import CustomerDetails from "./Components/CustomerAddressDetails/CustomerAddressHome"
import AboutUs from "./Components/AboutUs/About"
import VendorAddService from "./Components/VendorAddNewService/VendorAddServiceHome";
import Privacy from "./Components/PrivacyPolicy/Privacy"
import PageNotFound from "./Pages/Pagenotfound"




function App() {

  const [user, setUser] = useState(null);
  const [userRole, setUserRole] = useState(null);

  useEffect(() => {
    const u = sessionStorage.getItem("user")
    u && JSON.parse(u) ? setUser(true) : setUser(false)
    const ur = sessionStorage.getItem("useRole")
    ur && setUserRole(ur) 
  }, [])

  useEffect(() => {
    sessionStorage.setItem("user", user)
    sessionStorage.setItem("useRole", userRole)
  }, [user,userRole])




  return (
    <div >
      <BrowserRouter>
        <Routes>
          <Route exact path="/login" element={<Login setUser={setUser} setUserRole={setUserRole} />} />
          <Route exact path="/signup" element={<Signup />} />
          <Route exact path="/" element={<Home />} />
          <Route exact path="/user-roles" element={<UserRoles />} />
          <Route exact path="/blogs" element={<BlogHome />} />
          <Route exact path="/blogs/single-blog" element={<SinglePost />} />
          <Route exact path="/vendor-options" element={<MasterServicesHome />} />
          <Route exact path="/master-services" element={<VendorsHome />} />
          <Route exact path="/vendors-list" element={<VendorsListHome />} />
          <Route exact path="/vendor-info" element={<VendorProfile />} />
          <Route exact path="/planners" element={<PlannerHome />} />
          <Route exact path="/vendor-details" element={<MultiStepForm />} />
          <Route exact path="/customer-details" element={<CustomerDetails />} />
          <Route exact path="/about" element={<AboutUs />} />

          {user && userRole !== "Vendor" && <Route exact path="/create-blog" element={<CreateBlog />} />}

          {user && userRole === "Vendor" && <Route exact path="/vendor-dashboard"
            element={<VendorProfileHome />} />}
          {user && userRole === "Vendor" && <Route exact path="/vendor-dashboard/orders"
            element={<OrdersHome />} />}
          {user && userRole === "Vendor" && <Route exact path="/vendor-dashboard/profile"
            element={<VendorDashboardProfile />} />}
          {user && userRole === "Vendor" && <Route exact path="/vendor-dashboard/products"
            element={<VendorDashboardProducts />} />}
          {user && userRole === "Vendor" && <Route exact path="/vendor-dashboard/add/service"
            element={<VendorAddService />} />}

          {user && userRole === "Customer" && <Route exact path="/customer-dashboard" element={<CustomerDashboardHome />} />}
          {user && userRole === "Customer" && <Route exact path="/customer-dashboard/booking" element={<CustomerOrders />} />}

          {/* <Route path="/booking-cart" element={<Cart />}  /> */}
          {user && userRole !== "Vendor" &&  <Route exact path="/booking-cart" element={<Cart />} />}
          {user && userRole !== "Vendor" &&  <Route exact path="/checkout" element={<Checkout />} />}

          {user && userRole === "Admin" && <Route exact path="/admin-dashboard" element={<AdminHome />} />}
          {user && userRole === "Admin" && <Route exact path="/admin-dashboard/users-list" element={<UserList />} />}
          {user && userRole === "Admin" && <Route exact path="/admin-dashboard/user" element={<User />} />}
          {user && userRole === "Admin" && <Route exact path="/admin-dashboard/add-user" element={<NewUser />} />}
          {user && userRole === "Admin" && <Route exact path="/admin-dashboard/vendors-list" element={<VendorsList />} />}
          {user && userRole === "Admin" && <Route exact path="/admin-dashboard/vendor" element={<Vendor />} />}
          {user && userRole === "Admin" && <Route exact path="/admin-dashboard/add-vendor" element={<NewVendor />} />}
          {user && userRole === "Admin" && <Route exact path="/admin-dashboard/booking-list" element={<BookingsList />} />}
          {user && userRole === "Admin" && <Route exact path="/admin-dashboard/categories-list" element={<CategoriesList />} />}

          <Route exact path="/privacy" element={<Privacy />} />
          <Route path="/*" element={<PageNotFound />} />


        </Routes>
      </BrowserRouter>
      <ToastContainer theme="colored" />
    </div>
  );
}

export default App;



