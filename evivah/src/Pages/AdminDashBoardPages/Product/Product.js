
import { Cancel, Pending, Verified } from "@mui/icons-material";
import axios from "axios";
import { useState } from "react";
import { useLocation } from "react-router-dom";
import Sidebar from "../../../Components/AdminDashboard/Sidebar/Sidebar";
import Topbar from "../../../Components/AdminDashboard/topbar/Topbar";
import Imageslider from "../../../Components/Imageslider/Imageslider";
import { EvivahURL } from "../../../Config/Config";
import "./Product.css";

const Product = () => {
    const location = useLocation()
    const service = location.state.service
    const [flag, setFlag] = useState(service.isApproved);
    const [loading, setLoading] = useState(false)

    const Approved = () => {
        const status = (flag === 1) ?
            (
                <div>
                    <Verified style={{ color: "green" }} />
                    {"Authorized Vendor"}
                </div>
            )
            : (flag === 0) ?
                (
                    <div>
                        <Cancel style={{ color: "red" }} />
                        {"Un Approved Vendor"}
                    </div>
                ) :
                (
                    <div>
                        <Pending style={{ color: "teal" }} />
                        {"Pending Vendor"}
                    </div>
                );
        return status;

    }

    const toIndianCurrency = (num) => {
        const curr = num.toLocaleString('en-IN', {
            style: 'currency',
            currency: 'INR'
        });
        return curr;
    };

    const statusHandler = (serviceId, statusflag) => {
        // const token = sessionStorage.token
        // const config = {
        //     headers: { "Authorization": `Bearer ${token}` }
        // };
        
        const accessToken = sessionStorage.getItem("token");

        const url = `${EvivahURL}/admin/vendor/${serviceId}?status=${statusflag}`
        const authAxios = axios.create({
          baseURL: url,
          headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
            Authorization: "Bearer " + accessToken,
          },
        });
      
        // const url=`http://localhost:8080/admin/vendor/45?status=1`
        axios.put(url).then((response) => {
            let result = response.data
            setFlag(statusflag)
        })
    }


    return (
        <div>
            <Topbar />
            <div className="admin-container">
                <Sidebar />
                <div className='product' >
                    <div className="product-title-container">
                        <div>
                            <h1 className="product-title">{service.brandName}</h1>
                            <span className='fw-light'>
                                <Approved />
                            </span>
                        </div>
                   
                    </div>
                    <div className="product-top">
                        <div className="product-top-left">
                            <Imageslider images={service.imgList} size="300px" />
                          
                        </div>
                        <div className="product-top-rigth">
                            <div className="product-info-bottom">
                                <div className="product-info-item">
                                    <span className="product-info-key">
                                        Email:
                                    </span>
                                    <span className="product-info-value">
                                        {service.email}
                                    </span>
                                </div>
                                <div className="product-info-item">
                                    <span className="product-info-key">
                                        Category:
                                    </span>
                                    <span className="product-info-value">
                                        {service.masterServiceName}
                                    </span>
                                </div>
                                <div className="product-info-item">
                                    <span className="product-info-key">
                                        Vendor name:
                                    </span>
                                    <span className="product-info-value">
                                        {service.firstName} {service.lastName}
                                    </span>
                                </div>
                                <div className="product-info-item">
                                    <span className="product-info-key">
                                        Price:
                                    </span>
                                    <span className="product-info-value">
                                        {toIndianCurrency(service.servicePrice)}
                                    </span>
                                </div>
                                <div className="product-info-item">
                                    <span className="product-info-key">
                                        City:
                                    </span>
                                    <span className="product-info-value">
                                        {service.vendorCity}
                                    </span>
                                </div>
                                <div className="product-info-item">
                                    <span className="product-info-key">
                                        Mobile:
                                    </span>
                                    <span className="product-info-value">
                                        {service.mobile}
                                    </span>
                                </div>

                            </div>
                            <div className="d-flex justify-content-end">
                                {
                                    flag !== 1 &&
                                    <button
                                        style={{ marginRight: "10px", borderRadius: "22px" }}
                                        type="button"
                                        class="btn btn-outline-success"
                                        onClick={() => statusHandler(service.serviceId, 1)}
                                    >
                                        Approve
                                    </button>
                                }
                                {
                                    flag !== 0 &&
                                    <button
                                        style={{ marginRight: "10px", borderRadius: "22px" }}
                                        type="button"
                                        class="btn btn-outline-danger"
                                        onClick={() => statusHandler(service.serviceId, 0)}
                                    >
                                        Reject
                                    </button>
                                }
                            </div>
                        </div>

                    </div>
                    <div className="product-bottom">
                        <form className="product-form">
                            <div className="product-form-left">
                                <label>Service Specification</label>
                                <p>{service.specification}</p>
                                <label>Service Details</label>
                                <p>
                                    {service.description}
                                </p>

                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>

    )
}

export default Product
