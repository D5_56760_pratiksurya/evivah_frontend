
import axios from "axios"
import "./User.css"
import { EvivahURL } from "../../../Config/Config";
import { toast } from "react-toastify";
import { useState } from "react"


const User = ({ profile }) => {
    const [email, setEmail] = useState("")
    const [mobile, setMobile] = useState("")
    const [city, setCity] = useState("")
    const [state, setState] = useState("")
    const [postalcode, setPostalcode] = useState("")
    const [address, setAddress] = useState("")

    const updateUser = () => {
        if (email.length === 0) {
            toast.warning('Please Enter Email')
        } else if (!(email.includes("@") && email.includes(".com"))) {
            toast.warning("Please Enter Valid Email")
        } else if (mobile.length === 0 || mobile.length !== 10 || isNaN(mobile)) {
            toast.warning("Please Enter Valid Mobile Number")
        }

        else if (state.length === 0) {
            toast.warning("Please Select State.");
        }
        else if (city.length === 0) {
            toast.warning("Please Select City.");
        }
        else if (postalcode.length !== 6) {
            toast.warning("Postalcode Should be of 6 digits.");
        }
        else if (address.length === 0) {
            toast.warning("Address Cannot be Empty.");
        }
        else if (address.length > 100) {
            toast.warning("Address cannot be more than 100 characters.");
        }
        else {

            const body={
                email,
                mobile,
                state,
                city,
                "pincode":postalcode,
                "addressLine":address
              }

            const id = sessionStorage.id
            const token = sessionStorage.token
            const config = {
                headers: { "Authorization": `Bearer ${token}` }
            };
            const url = `${EvivahURL}/user/update/details/${id}`

            axios.put(url, body,config).then((response) => {
                const result = response.data
                if (result.status === "success") {
                    toast.success("Profile Updated");
                }
                else {
                    toast.success("Profile Updatation Failed");
                }
            })
        }

    }



    return (
        <div>

            <div className="admin-container">

                <div className="user" >

                    <div className="user-container">

                        <div style={{ background: "#fff" }} className="user-update">
                            <span className="user-update-title">
                                Edit
                            </span>
                            <form action="" className="user-update-form">
                                <div className="user-update-left">

                                    <div className="user-update-item">
                                        <label>Email</label>
                                        <input onChange={(e) => setEmail(e.target.value)} type="text" placeholder={profile.email}
                                            className="user-update-input" />
                                    </div>

                                    <div className="user-update-item">
                                        <label>Mobile Number</label>
                                        <input onChange={(e) => setMobile(e.target.value)} type="number" placeholder={profile.mobile}
                                            className="user-update-input" />
                                    </div>

                                    <div className="user-update-item">
                                        <label>State</label>
                                        <input onChange={(e) => setState(e.target.value)} type="text" placeholder={profile.state}
                                            className="user-update-input" />
                                    </div>


                                    <div className="user-update-item">
                                        <label>City</label>
                                        <input onChange={(e) => setCity(e.target.value)} type="text" placeholder={profile.city}
                                            className="user-update-input" />
                                    </div>

                                    <div className="user-update-item">
                                        <label>Postal Code</label>
                                        <input onChange={(e) => setPostalcode(e.target.value)} type="text" placeholder={profile.pincode}
                                            className="user-update-input" />
                                    </div>


                                    <div className="user-update-item">
                                        <label>Address</label>
                                        <input onChange={(e) => setAddress(e.target.value)} type="text" placeholder={profile.addressLine}
                                            className="user-update-input" />
                                    </div>
                                </div>

                                <div className="user-update-rigth">
                                    <button onClick={updateUser} type="button" class="user-update-btn">Update</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    )
}

export default User
