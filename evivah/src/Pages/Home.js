import Navbar from '../Components/Navbar/Navbar'
import Showcase from '../Components/Showcase/Showcase'
import Categories from '../Components/Categories/ServiceCategories'
import Featured from '../Components/FeaturedVendors/FeaturedVendorsItems'
import Blogs from '../Components/LatestBlogs/Blogs'
import VendorRegistration from '../Components/HomeVendorRegistration/VendorRegistration'
import Footer from '../Components/Footer/Footer'
import { useCallback, useEffect, useState } from 'react'
import axios from 'axios'
import {EvivahURL} from "../Config/Config"


const Home = () => {

    const [masterServiceList, setmasterServiceList] = useState([])
    const [featuredVendorsList, setfeaturedVendorsList] = useState([])
    const [latestBlogList, setlatestBlogList] = useState([])
   
   
    const getmasterServices = useCallback(async () => {
        const url = `${EvivahURL}/masterServices`
        const data = axios.get(url).then((response) => {
            let result = response.data.data
            setmasterServiceList(result)
        })


    }, [])
    const getFeaturedVendors = useCallback(async () => {
       
        const url = `${EvivahURL}/vendor/featured`
        const data = axios.get(url).then((response) => {
            let result = response.data.data
            setfeaturedVendorsList(result)
        })


    }, [])


    const getLastestBlogs = useCallback(async () => {
       

        const url = `${ EvivahURL}/blog/latest`
        const data = axios.get(url).then((response) => {
            let result = response.data.data
            setlatestBlogList(result)
        })


    }, [])



    useEffect(() => {
        getmasterServices()
        getFeaturedVendors()
        getLastestBlogs()
    }, [getmasterServices,getFeaturedVendors,getLastestBlogs])



    return (
        <div>
            <Navbar />
            <Showcase />
            <Categories masterServiceList={masterServiceList} />
            <Featured data={featuredVendorsList} />
            <Blogs blogData={latestBlogList} />
            <VendorRegistration />
            <Footer />
        </div>
    );
}

export default Home
