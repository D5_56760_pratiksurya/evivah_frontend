import axios from 'axios'
import React, { useEffect, useState } from 'react'
import { Col, Row } from 'react-bootstrap'
import ShowProfile from '../Components/ClientDashBoard/ShowProfile/ShowProfile'
import Sidebar from "../Components/CustomerDashboard/Sidebar/SideBar"
import EditProfile from '../Pages/AdminDashBoardPages/user/User'
import Navbar from '../Components/Navbar/Navbar'
import { EvivahURL } from '../Config/Config'

const CustomerDashboardHome = () => {
    const [profile, setProfileData] = useState({})
    const [loading, setLoading] = useState(false)

    const showProfile = async () => {
        const id = sessionStorage.id
        const token = sessionStorage.token

        const config = {
            headers: { "Authorization": `Bearer ${token}` }
        };

        const url = `${EvivahURL}/user/get/${id}`

        const data = await axios.get(url, config).then((response) => {
            const result = response.data
            setProfileData(result.data)
            setLoading(true)
        })
    }

    useEffect(() => {
        showProfile()
    }, [])


    return (
        <div>
            <Navbar></Navbar>
            <div>
                 <Row style={{ width: "100%" }}>
                    <Col md={2}>
                        <Sidebar />
                    </Col>
                    {loading && <Col md={10} style={{ paddingTop: "30px" }}>
                        <Row>
                            <Col>
                                <ShowProfile profile={profile} />
                            </Col>
                            <Col>
                                <EditProfile profile={profile} />
                            </Col>
                        </Row>
                    </Col>}
                </Row>
            </div>
        </div>
    )
}

export default CustomerDashboardHome