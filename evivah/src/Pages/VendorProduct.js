import { Grid } from '@mui/material';
import axios from 'axios';
import React, { useEffect, useState } from 'react';
import { Container } from 'react-bootstrap';
import Service from '../Components/ClientDashBoard/Service/Service';
import SideBar from '../Components/ClientDashBoard/Sidebar/SideBar';
import Navbar from '../Components/Navbar/Navbar';
import { EvivahURL } from "../Config/Config";

const VendorProduct = () => {
    const [services,setServices]=useState([])
 
    

    const getAllServices=()=>{
        const id=sessionStorage.id
        const token = sessionStorage.token
      const config = {
        headers: { "Authorization": `Bearer ${token}` }
    };
        const url = `${EvivahURL}/vendor/${id}/services`

        axios.get(url,config).then((response)=>{
            const result=response.data
            setServices(result.data)
        })
    }

    

    useEffect(()=>{
        getAllServices()
    },[])

    return (
        <div>
            <Navbar />
            <div
                className='d-flex'
                style={{ backgroundColor: "#E2F0FE",width:"100%" }}
            >
                <Grid container>
                    <Grid item md={2} sm={3} xs={2}>
                        <SideBar />
                    </Grid>
                    <Grid item sm={9} md={10} xs={10}>
                        <Container style={{padding:"0 30px",width:"100%"}} className="py-3" >
                            <Service products={services} />
                        </Container>
                    </Grid>
                </Grid>
            </div>
        </div>
    );
};

export default VendorProduct;
