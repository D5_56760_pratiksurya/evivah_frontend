import React from 'react'
//import pagenotfound image
import pagenotfoundImage from "../img/pagenotfound.jpg"
//react router dom
import { BrowserRouter as Router, Route, Switch, Link, Redirect } from 'react-router-dom';




const PageNotFound = () => {
    return (
        <div style={{ textAlign: "center" }} className="pageNotFound">

            <h1 style={{ textAlign: "center" }}>Oops..! 404 Page Not Found</h1>
            <p>Looks like you came to wrong page on our server</p>
            <div className='d-flex align-items-center flex-column'>
                <img src={pagenotfoundImage} height="500" width="500" alt="not found" />
                <button className='btn btn-secondary'>
                 <Link style={{color:"#fff"}} to="/">Home</Link>
                </button>
            </div>
        </div>


    )
}

export default PageNotFound;