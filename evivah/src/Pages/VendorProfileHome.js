import AddIcon from '@mui/icons-material/Add';
import { Grid } from '@mui/material';
import { Fab, Tooltip } from '@mui/material';
import axios from 'axios';
import React, { useEffect, useState } from 'react';
import Home from '../Components/ClientDashBoard/Home/DashboardHome';
import SideBar from '../Components/ClientDashBoard/Sidebar/SideBar';
import Navbar from '../Components/Navbar/Navbar';
import { EvivahURL } from "../Config/Config";



const fab = (theme) => ({
    position: "fixed",
    right: 20,
    bottom: 20

})


const VendorProfile = () => {

    const [homeData, setHomeData] = useState([])
    const [serviceCount,setServiceCount]=useState(0)
    

    const getMasterServices = () => {
        const userId = sessionStorage.id
        const token = sessionStorage.token
        const config = {
            headers: { "Authorization": `Bearer ${token}` }
        };
        const url = `${EvivahURL}/orders/vendor/user/${userId}`
        axios.get(url,config).then((response) => {
            let result = response.data
           
            setHomeData(result.data)
        })
    }

    const getServices=()=>{
        const userId = sessionStorage.id
        const token = sessionStorage.token
        const config = {
            headers: { "Authorization": `Bearer ${token}` }
        };
        const url = `${EvivahURL}/vendor/${userId}/services`
        axios.get(url,config).then((response) => {
            let result = response.data
            setServiceCount(result.data.length)
        })
    }

    useEffect(() => {
        getServices()
    }, [])

    useEffect(() => {
        getMasterServices()
    }, [])




    return (

        <div>
            <Navbar />
            <div
                className='d-flex'
                style={{ backgroundColor: "#E2F0FE" }}
            >
                <Grid container style={{ width: "100%" }}>
                    <Grid item md={2} sm={3} xs={2}>
                        <SideBar />
                    </Grid>
                    <Grid item sm={9} md={10} xs={10}>
                        <Home serviceCount={serviceCount} homeData={homeData} />

                    </Grid>
                </Grid>
            </div>

           

        </div>

    );
};

export default VendorProfile;

