import "./Topbar.css"
import logo from "../../../img/websiteLogo1.png"
import defaultLogo from "../../../img/blogDefault.png"
import { Link } from "react-router-dom"
import {  useDispatch } from 'react-redux';
import { emptyCart } from "../../../actions/index"
import { useNavigate } from 'react-router-dom'
import { useCallback, useEffect } from "react";
import { EvivahURL } from "../../../Config/Config";
import axios from "axios";

const Topbar = () => {

    const navigate = useNavigate();

    const dispatch=useDispatch()

    const logoutHandler = () => {
        sessionStorage.removeItem('id')
        sessionStorage.removeItem('firstName')
        sessionStorage.removeItem('lastName')
        sessionStorage.removeItem('loginStatus')
        sessionStorage.removeItem('email')
        sessionStorage.removeItem('role')
        sessionStorage.removeItem('token')

        sessionStorage.removeItem("user")
        sessionStorage.removeItem("userRole")

        dispatch(emptyCart())

        navigate('/login')
    }

    return (
        <div className="topbar" >
            <div className="topbar-wrapper">
                <div className="top-left" style={{ height: "100%" }}>
                    <Link to="/">
                        <img src={logo} alt="" style={{ height: "100%" }} />
                    </Link>

                </div>
                <div className="top-right">
                    <div onClick={logoutHandler} style={{paddingRight:"10px",cursor:"pointer"}}>
                        Log Out
                    </div>
                    <img src={defaultLogo} className="topAvatar" alt="" />

                </div>

            </div>
        </div>
    )
}

export default Topbar
