import { PhotoCamera } from '@mui/icons-material'
import React, { useState } from 'react'

const VendorAddService = ({imgFiles,setImgFiles}) => {
    const [previews, setPreviews] = useState([])


    const uploadHandler = (e) => {
        const fileList = Array.from(e.target.files)
        setImgFiles(fileList)


        const mappedFiles = fileList.map((file) => ({
            ...file,
            preview: URL.createObjectURL(file),
        }))

        setPreviews(mappedFiles)
    }

    const showPreviews = (previewsList) => {

        return (previewsList.map((photo) => {
            return (
                <img style={{ width: "250px", height: "250px", objectFit: "cover" }} src={photo.preview} alt="" />
            )
        }))
    }
    return (
        <div>
            <div style={{ textAlign: "center" }}>
                <h3 style={{ paddingBottom: "20px" }} >Upload Photos to Display Your Service</h3>
                <div >
                    <div className='pb-4'>
                        <label htmlFor="icon-button-file">
                            Upload Photo:
                            <input
                                multiple
                                onChange={uploadHandler}
                                accept="image/*"
                                style={{ display: "none" }}
                                id="icon-button-file" type="file"
                            />
                            <PhotoCamera color="primary" sx={{ marginLeft: "6px", cursor: "pointer" }} />
                        </label>
                    </div>

                    <div>
                        {previews && showPreviews(previews)}
                    </div>



                </div>
            </div>
        </div>
    )
}

export default VendorAddService