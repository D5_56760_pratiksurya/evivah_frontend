import React from 'react'
import './privacy.css'

const privacy = () => {
    return (
      <div>
        <section className='privacy'>
          <div className='privacy-overlay'>
            <h1>Privacy Policy</h1>
          </div>
          <div className="privacy-policy">
            <section>
             <p>This privacy policy applies to your use of the Evivah website located at www.Evivah.com (hereafter known as Evivah),but not does apply to any third party sites that may be linked to them, or any relationships you may have with the businesses listed on Evivah. The terms "we", "us", and "Evivah" refer to Evivah.com and the terms "you" and "your" refer to you, as a user of Evivah. The term "personal information" means information that you provide to us which personally identifies you to be contacted or identified, such as your name, phone number, email address, and any other data that is tied to such information. By using Evivah, you agree to be bound by terms of this privacy policy.</p>
            </section>
            <section>
                <h4>1. Information we collect and how we use it</h4>
                <p>
                    We use commercially reasonable efforts to ensure that the collection of personal information is limited to that which is necessary to fulfill the purposes identified below. If we use your information in a manner different than the purpose for which it is collected, then we will ask you for your consent prior to such use. Account information. If you create an account to take advantage of the full range of services offered on Evivah, we ask for and record personal information such as your name and email address. We use your email address to send you updates, news, and newsletters (if you willingly subscribe to the newsletter during signup, or anytime after sign up) and contact you on behalf of other users (such as other users who send you compliments, personal messages, or favorite reviews). Telephone numbers. Any telephone number entered as part of the account information and which has not downloaded will not be shared with any third party. However, Evivah assures that this information will not be made public or sold to any third party. Only the listed business contact info will be made public on the will of proprietor of that business. Evivah is not liable for misuse of this information by these specific businesses or persons related to these businesses. Submissions. We store and may publicly display your submissions in order to provide the services that we offer. When we display your submissions, we do so together with your account name (first name and last initial). The term "submissions" refers to the information that you submit or post to Evivah for public display, such as business ratings, reviews, photos, compliments, and the information that you display as part of your account profile. Feedback. If you contact us to provide feedback, register a complaint, or ask a question, we will record any personal information and other content that you provide in your communication so that we can effectively respond to your communication. Activity. We record information relating to your use of Evivah, such as the searches you undertake, the pages you view, your browser type, IP address, location, requested URL, referring URL, and timestamp information. We use this type of information to administer Evivah and provide the highest possible level of service to you. We also use this information in the aggregate to perform statistical analyses of user behavior and characteristics in order to measure interest in and use of the various areas of Evivah. You cannot be identified from this aggregate information. Cookies. We send cookies to your computer in order to uniquely indentify your browser and improve the quality of our service. The term "cookies" refers to small pieces of information that a website sends to your computer's hard drive while you are viewing the site. We may use both session cookies (which expire once you close your browser) and persistent cookies (which stay on your computer until you delete them). Persistent cookies can be removed by following your browser help file directions. If you choose to disable cookies, some areas of Evivah may not work properly or at all. Evivah uses third party tools, who may collect anonymous information about your visits to Evivah using cookies, and interaction with Evivah products and services. Such third parties may also use information about your visits to Evivah products and services and other web sites to target advertisements for Evivah products and services. These third parties may have access to the name, phone number, address, email address, or any personally identifying information about Evivah users. Please refer to the help file of your browser to understand the process of deactivating Cookies on your browser. Enforcement. We may use the information we collect in connection with your use of Evivah(including your personal information) in order to investigate, enforce, and apply our Terms of Service and Privacy Policy.
                </p>
            </section>
            <section>
                <h4>
                2.Security
                </h4>
                <p>Your account is password protected. We use industry standard measures to protect the personal information that is stored in our database. We limit the access to your personal information to those employees and contractors who need access to perform their job function, such as our customer service personnel. If you have any questions about the security on Evivah please contact us. Although we take appropriate measures to safeguard against unauthorized disclosures of information, we cannot assure you that your personal information will never be disclosed in a manner that is inconsistent with this Privacy Policy. You hereby acknowledge that Evivah is not responsible for any intercepted information sent via the internet, and you hereby release us from any and all claims arising out of or related to the use of intercepted information in any unauthorized manner.</p>
            </section>
            <section>
                <h4>
                3.Terms and modifications to this Privacy Policy
                </h4>
                <p>
                We may modify this Privacy Policy at any time, and we will post any new versions on this page. If we make any material changes in the way we use your personal information, we will notify you by sending an e-mail to the last e-mail address you provided to us and/or by prominently posting notice of the changes on our website. Any such changes will be effective upon the earlier of thirty (30) calendar days following our dispatch of an e-mail notice to you or thirty (30) calendar days following our posting of notice of the changes on our website. These changes will be effective immediately for new users of Evivah Please note that at all times you are responsible for updating your personal information to provide us with your most current e-mail address. In the event that the last e-mail you have provided us is not valid, or for any reason is not capable of delivering to you the notice described above, our dispatch of the e-mail containing such notice will nonetheless constitute effective notice of the changes described in the notice. In any event, changes to this Privacy Policy may affect our use of personal information that you provided us prior to our notification to you of the changes. If you do not wish to permit changes in our use of your personal information, you must notify us prior to the effective date of the changes that you wish to deactivate your account with us. Continued use of Evivah following notice of such changes shall indicate your acknowledgement of such changes and agreement to be bound by the terms and conditions of such changes.
                </p>
            </section>
          </div>
        </section>
        
      </div>
  
    );
  }
  
  export default privacy