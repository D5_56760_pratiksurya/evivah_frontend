import React, { useState } from "react";
import CallIcon from '@mui/icons-material/Call';
import Modal from '@mui/material/Modal';
import { Box } from "@mui/material";
import { toast } from "react-toastify";

const style = {
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: 400,
  bgcolor: 'background.paper',
  border: '2px solid #000',
  boxShadow: 24,
  p: 4,
};

const Calldetails = ({ email, mobile }) => {
  const [open, setOpen] = useState(false);

  const user = sessionStorage["loginStatus"]
  const handleOpen = () => {
    if(!user){
      toast.error("Please Login to get Contact Details")
    }
    else{
      setOpen(true)
    }
  };
  const handleClose = () => setOpen(false);
  return (
    <div>
      <button
        onClick={handleOpen}
        style={{
          width: "100%",
          marginLeft: "12px",
          borderRadius: "30px",
          backgroundColor: "#fa4a6f",
          border: "none",
        }} type="button" className="btn btn-primary" 
      >

        <CallIcon /> Call
      </button>
      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style}>
          <div style={{ textAlign: "center" }}> <b> Email </b> : {email}</div>

          <div style={{ textAlign: "center" }}> <b>Mobile Number </b>:{mobile}</div>
        </Box>
      </Modal>
    </div>
  );
};

export default Calldetails;
