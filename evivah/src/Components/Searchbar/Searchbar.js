import React, { useState } from 'react'
import { useEffect } from 'react'

const Searchbar = ({ setVendorsFilterList, vendorsList }) => {
    const [search, setSearch] = useState("")

    const searchByCityandBrand = (e) => {
        setSearch(e.target.value)

        const filtered = vendorsList.filter((vendor) => {
            return (vendor.vendorCity.toLowerCase().includes(search.toLowerCase()) || vendor.brandName.toLowerCase().includes(search.toLowerCase()))
        })
        setVendorsFilterList(filtered)

    }

    useEffect(()=>{
        if(search.length===0){
            setVendorsFilterList(vendorsList)
            setSearch("")
        }
    },[search])

    return (
        <div>
            <div className="input-group mb-3">
                <input
                    type="text"
                    className="form-control"
                    aria-label="Text input with segmented dropdown button"
                    placeholder='search by city or brandname ...'
                    onChange={(e) => searchByCityandBrand(e)}
                ></input>

                <button type="button" className="btn btn-outline-secondary">
                    Search
                </button>
            </div>
        </div>
    )
}

export default Searchbar