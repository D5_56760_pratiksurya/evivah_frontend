import * as React from 'react';
import Box from '@mui/material/Box';
import Modal from '@mui/material/Modal';
import { TextField } from '@mui/material';
import { useState } from 'react';
import { toast } from 'react-toastify';
import axios from 'axios';
import { EvivahURL } from '../../Config/Config';
import { Link } from 'react-router-dom';

const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 400,
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
};

function ForgotPassword() {
    const [open, setOpen] = useState(false);
    const [email, setEmail] = useState("");
    const [otp, setOtp] = useState("");
    const [newPassword, setNewPassword] = useState("");
    const [isValidUser, setIsValidUser] = useState(false);
    const [isValidField,setIsValidField]=useState(false)

    const handleOpen = () => setOpen(true);
    const handleClose = () => setOpen(false);

    const submitHandler = () => {

        if (otp.length === 0 ) {
            toast.warning('Otp Cannot be empty')
        }
        else if (newPassword.length === 0) {
            toast.warning('Password Cannot be empty')
        }

        else {

            const body = {
                email,
                otp,
                password:newPassword,
            }



            const url = `${EvivahURL}/user/change/password`
            setIsValidField(!true)
            axios.put(url, body).then((response) => {
                const result = response.data
                setOpen(false)
                if (result["status"] === "success") {
                    toast.success("Password Changed Successfully")
                    isValidUser(false)
                }
                else {
                    toast.error("Please Enter Valid OTP")
                    isValidUser(false) 
                }
            })
        }
    }

    const emailValidation = () => {
        if (email.length === 0) {
            toast.warning('Please Enter Email Addreess')
        }
        else {
            const url = `${EvivahURL}/user/forgotPass/${email}`
            setIsValidField(true)
            axios.put(url).then((response) => {
                const result = response.data
                if (result["status"] === "success") {
                    toast.success("Otp Sent to your email...")
                    setIsValidUser(true)
                }
                else {
                    toast.error("Please Enter Valid Email")
                }
            })
        }
    }

    return (
        <div>
            <button onClick={handleOpen} style={{ textDecoration: "none" }} type="button" className="btn btn-link">Forgot Password</button>
            <Modal
                open={open}
                onClose={handleClose}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
            >
                <Box sx={style}>
                    <TextField
                        fullWidth
                        id="standard-number"
                        label="Enter Email"
                        type="text"
                        variant="standard"
                        onChange={(e) => setEmail(e.target.value)}
                        sx={{
                            marginBottom: "20px"
                        }}
                    />
                    {
                    isValidUser && (
                        <>
                            <TextField
                                fullWidth
                                id="standard-number"
                                label="Enter OTP"
                                type="text"
                                variant="standard"
                                onChange={(e) => setOtp(e.target.value)}
                                sx={{
                                    marginBottom: "20px"
                                }}
                            />
                            <TextField
                                fullWidth
                                id="standard-number"
                                label="Enter New Password"
                                type="text"
                                variant="standard"
                                onChange={(e) => setNewPassword(e.target.value)}
                                sx={{
                                    marginBottom: "20px"
                                }}
                            />
                            <div style={{ textAlign: "center" }} >
                                <button disabled={!isValidField}  onClick={submitHandler}
                                    style={{ borderRadius: "5px", background: "rgb(0, 0, 111)",color:"#fff" }} className="p-2 btn" >
                                    Change Password
                                </button>
                            </div>
                        </>
                    )
                    }
                    {!isValidUser && <div style={{ textAlign: "center" }} >
                        <button disabled={isValidField} onClick={emailValidation}
                            style={{ borderRadius: "5px", background: "rgb(0, 0, 111)",color:"#fff" }} className="p-2 btn" >
                            Send Otp
                        </button>
                    </div>}
                </Box>
            </Modal>
        </div>
    );
}

export default ForgotPassword