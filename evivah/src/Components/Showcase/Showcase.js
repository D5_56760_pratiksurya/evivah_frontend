import React from 'react'
import { useNavigate } from 'react-router-dom';
import './showcase.css'

const Showcase = () => {
  const navigate=useNavigate()
  return (
    <section className='showcase'>
      <div className='showcase-overlay'>
        <h1>Plan Your Dream Wedding</h1>
        <button type="button" onClick={()=>navigate("/vendor-options")} className="btn btn-danger showcase-btn">Start Your Journey</button>
     </div>
    </section>
  );
}

export default Showcase