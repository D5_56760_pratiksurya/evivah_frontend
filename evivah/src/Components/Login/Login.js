
import "./login.css"
import { useState } from "react";
import { toast } from "react-toastify";
import { useNavigate } from "react-router-dom";
import axios from "axios";
import Navbar from "../Navbar/Navbar";
import { useDispatch } from "react-redux"
import { isLogged } from "../../actions/index"
import { EvivahURL } from "../../Config/Config";
import { Spinner } from "react-bootstrap";
import LoginImg from "../../img/loginImg.jpg"
import { Link } from "react-router-dom";
import ForgotPassword from "../ForgorPassword/ForgotPassword"


const Login = ({ setUser, setUserRole }) => {


  const dispatch = useDispatch()




  const [email, setEmail] = useState("")
  const [password, setPassword] = useState("")

  const [isValid, setIsValid] = useState(false)



  const navigate = useNavigate()


  const LoginHandler = () => {

    if (email.length === 0) {
      toast.warning('please enter email')
    } else if (password.length === 0) {
      toast.warning('please enter password')
    }
    else if (!(email.includes("@") && email.includes(".com"))) {
      toast.warning("please enter valid email")
    }
    else {
      const body = {
        email,
        password,
      }
      const url = `${EvivahURL}/user/signin`
      axios.post(url, body).then((response) => {
        const result = response.data
        if (result['status'] === 'success') {
          setIsValid(false)
          setUser(true)
          dispatch(isLogged())

          const { id, firstName, lastName, email, role } = result['data']
          setUserRole(role)
          const token = result["token"]
          toast.success('Welcome, ' + firstName + " " + lastName)


          sessionStorage['id'] = id
          sessionStorage['firstName'] = firstName
          sessionStorage['lastName'] = lastName
          sessionStorage['email'] = email
          sessionStorage['loginStatus'] = 1
          sessionStorage['role'] = role
          sessionStorage['token'] = token




          if (role === "Customer") {
            navigate('/')
          }
          else if (role === "Vendor" || role === "Planner") {

            navigate("/")
          }
          else if (role === "Admin") {
            navigate("/")
          }

        } else {
          setIsValid(false)
          toast.error('Invalid user name or password')
        }

      })
    }
  }



  return (
    <>
      <Navbar />
      <div className="login">
        <h1 className="loginTitle">Choose a Login Method</h1>
        <div className="wrapper">
          <div style={{ width: "100%", height: "100%" }} className="left" >
            <img style={{ width: "100%", height: "100%" }} src={LoginImg} alt="" />
          </div>
          <div className="right">
            <input onChange={(e) => setEmail(e.target.value)} className="loginInput" type="text" placeholder="Email" />
            <input onChange={(e) => setPassword(e.target.value)} className="loginInput" type="password" placeholder="Password" />
            <button disabled={isValid} onClick={LoginHandler} className="submit btn">Login</button>
            <div className="d-flex flex-column pt-2 align-items-center">
              <ForgotPassword />
              <Link to="/user-roles">New Here? Please Signup Here</Link>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default Login;