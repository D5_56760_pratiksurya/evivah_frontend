import React from 'react';
import CardItem1 from './FeaturedVendorsCard';
import './style.css';


function Cards1({data}) {



  return (
    <div className='vendorcards'>
      <h1>Top Featured Vendors</h1>
      <div className='vendorcards__container'>
        <div className='vendorcards__wrapper'>

          <ul className='vendorcards__items'>
            {
              data.map((card,index)=>{
                return (
                  <CardItem1 key={index} vendor={card} ></CardItem1>
                )
              })
            }
          
          </ul>
        </div>
      </div>
    </div>
  );
}

export default Cards1;