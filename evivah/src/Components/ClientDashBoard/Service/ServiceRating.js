import { Rating } from '@mui/material';
import StarIcon from '@mui/icons-material/Star';
import React from 'react';

const ServiceRating = ({ rating }) => {
    const ratingArray = rating && rating.map((feedback) => feedback.rating)
    const avgRating = ratingArray.length !== 0 ? (ratingArray.reduce((rating, b) => rating + b) / ratingArray.length) : 0
    return (


        <div >
            < Rating
                name="text-feedbacks"
                value={avgRating}
                readOnly
                precision={0.5}
                emptyIcon={<StarIcon style={{ opacity: 0.55 }} fontSize="inherit" />}
            />
        </div >

    )
}

export default ServiceRating



















