package com.wedding;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;


@SpringBootApplication(exclude = { SecurityAutoConfiguration.class })
public class ProjectW2Application {

	
	
	public static void main(String[] args) {
		SpringApplication.run(ProjectW2Application.class, args);
	}
	
	
//	
//	@EventListener(ApplicationReadyEvent.class)
	
//	public void sendMail() {
//		emailService.sendEmail("pssurya29@gmail.com", "This is testing gmail subject", "This is body od gmail for testing");
//	}
//	
}
