package com.wedding.services;

import java.util.DoubleSummaryStatistics;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.amazonaws.services.kms.model.NotFoundException;
import com.wedding.daos.IVendorServiceDetailsDao;
import com.wedding.dtos.DtoEntityConverter;
import com.wedding.dtos.VendorServiceDetailsDto;
import com.wedding.entities.VendorServiceDetails;

@Transactional
@Service
public class VendorServiceDetailsImpl {
	@Autowired
	private IVendorServiceDetailsDao serviceDetailsDao;
	@Autowired
	private DtoEntityConverter converter;
	@Autowired
	private BookingServiceImpl bookingService;
	@Autowired
	private EmailSenderServiceImpl emailSender;

	public VendorServiceDetailsDto findServiceDetailsById(int id) {
		VendorServiceDetails serviceDetails = serviceDetailsDao.findByVendorServiceDetailsId(id);

		return converter.toVendorServiceDetailsDto(serviceDetails);

	}

	public List<VendorServiceDetailsDto> findServiceDetailsByVendorId(int vendorId) {

		List<VendorServiceDetails> allServices = serviceDetailsDao.findByUserId(vendorId);
		List<VendorServiceDetailsDto> dto = null;
		if (!allServices.isEmpty()) {
			dto = allServices.stream().map((service) -> converter.toVendorServiceDetailsDto(service))
					.collect(Collectors.toList());
		} else {
			throw new NotFoundException("Vendor Not Found. Check vendor Id");
		}

		return dto;

	}

	public String saveServiceDetails(VendorServiceDetailsDto vsDetails) {
		VendorServiceDetails details = converter.toVendorServiceDetailsEntity(vsDetails);
		serviceDetailsDao.save(details);
		return "Service Added....";
	}

	public int deleteServiceDetails(int id) {
		if (serviceDetailsDao.existsById(id)) {
			serviceDetailsDao.deleteById(id);
			return 1;
		}
		return 0;
	}

	public String vendorServiceApproval(int serviceId, int flag) {

		int isApproved = serviceDetailsDao.approvedService(serviceId, flag);
		VendorServiceDetailsDto dto = findServiceDetailsById(serviceId);
		String email = dto.getEmail();
		String firstName = dto.getFirstName();
		String brandName = dto.getBrandName();
		String.format("<b>%s</b>", brandName);

		String isApprovedMsg = "\rWelcome to Evivah!\n Your Service " + brandName
				+ " has been successfully approved. Start enjoying all facilities from our platform and grow you buisness."
				+ " \n Please follow community guidelines and privacy policy.";

		String isRejectedMsg = " \r\n This is is to inform you your application for registration of " + brandName
				+ " has been rejected. Please read our documents and reapply, we are eagerly waiting to welcome you in our Evivah family";

		StringBuilder emailmsg = new StringBuilder();

		if (isApproved == 1 && flag == 1) {

			emailmsg.append("\rHey " + firstName + ",\r\n" + isApprovedMsg
					+ "\n\nYou're now part of a global community connecting wedding vendors, planners, and brides and grooms to-be across the world.\r\n"
					+ "We hope you're pleased by what you see in Evivah\r\n" + "\r\n"
					+ "For an even more customizable experience, tell us a little bit more about yourself.\r\n"
					+ "\r\n");
			emailmsg.append("\n\n\n\nRegards, Evivah Team");

			

			try {
				emailSender.sendSimpleEmail(email, emailmsg.toString(),
						"Regarding your application for registraion to Evivah");
			} catch (MessagingException e) {
				e.printStackTrace();
			}

			return "Approved Successfully";
		} else if (isApproved == 1 && flag == 0) {

			emailmsg.append("Hey " + firstName + ",\r\n" + isRejectedMsg);
			emailmsg.append("\n\n\n\nRegards, Evivah Team");
			
			try {
				emailSender.sendSimpleEmail(email, emailmsg.toString(),
						"Regarding your application for registraion to Evivah");
			} catch (MessagingException e) {
				e.printStackTrace();
			}
			return "Rejected Successfully";
		}
		return null;

	}

	public List<VendorServiceDetailsDto> getAllVendors() {
		List<VendorServiceDetails> allServices = serviceDetailsDao.findAll();
		List<VendorServiceDetailsDto> dto = allServices.stream()
				.map(service -> converter.toVendorServiceDetailsDto(service)).collect(Collectors.toList());
		return dto;
	}

	public List<VendorServiceDetailsDto> getAllServices() {
		List<VendorServiceDetails> allServices = serviceDetailsDao.findAll();
		List<VendorServiceDetailsDto> dto = allServices.stream()
				.map(service -> converter.toVendorServiceDetailsDto(service))
				.filter(service -> !service.getMasterServiceName().equalsIgnoreCase("planner"))
				.collect(Collectors.toList());
		return dto;
	}

	public List<VendorServiceDetailsDto> getAllPlanners() {
		List<VendorServiceDetails> allServices = serviceDetailsDao.findAll();
		List<VendorServiceDetailsDto> dto = allServices.stream()
				.map(service -> converter.toVendorServiceDetailsDto(service))
				.filter(service -> service.getMasterServiceName().equalsIgnoreCase("planner"))
				.collect(Collectors.toList());
		return dto;
	}

	public long getVendorsCount() {
		return getAllServices().stream().count();
	}

	public long getPlannersCount() {

		return getAllPlanners().stream().count();
	}

	public HashMap<String, Long> getHomeStats() {

		HashMap<String, Long> homeStats = new HashMap<>();

		long vendorCount = getVendorsCount();
		long plannerCount = getPlannersCount();
		DoubleSummaryStatistics bookingStats = bookingService.getAllBookingsStatistics();
		long bookingCount = bookingStats.getCount();
		long bookingSales = (long) bookingStats.getSum();

		homeStats.put("vendorCount", vendorCount);
		homeStats.put("plannerCount", plannerCount);
		homeStats.put("bookingCount", bookingCount);
		homeStats.put("bookingSales", bookingSales);

		return homeStats;

	}

	public List<VendorServiceDetailsDto> lastFiveVendors() {
		List<VendorServiceDetails> vendors = serviceDetailsDao.findTop5ByOrderByCreatedTimestampDesc();

		List<VendorServiceDetailsDto> dto = vendors.stream()
				.map(service -> converter.toVendorServiceDetailsDto(service)).collect(Collectors.toList());

		return dto;
	}

	public List<VendorServiceDetailsDto> featuredvendors() {
		List<VendorServiceDetails> vendors = serviceDetailsDao.featuredvendors();
		List<VendorServiceDetailsDto> dto = vendors.stream()
				.map(service -> converter.toVendorServiceDetailsDto(service)).collect(Collectors.toList());

		return dto;
	}

}
