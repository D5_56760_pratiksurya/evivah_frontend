package com.wedding.dtos;

import java.io.Serializable;

public class Credentials implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -2125691413887791024L;
	private String email;
	private String password;
	private int otp;
	
	public Credentials(String email, String password) {
		super();
		this.email = email;
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	

	public int getOtp() {
		return otp;
	}

	public void setOtp(int otp) {
		this.otp = otp;
	}

	@Override
	public String toString() {
		return String.format("Credentials [email=%s, password=%s]", email, password);
	}
	
	
	
	
	
	
}
